package com.example.demo1.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo1.exceptions.RecordNotFoundException;
import com.example.demo1.modal.User;
import com.example.demo1.repository.CurdRepository;

@Service
public class UserService {
	
	@Autowired
	CurdRepository repo;
	
	public User addUser(String name) {
		User u = new User();
		u.setName(name);
		return repo.save(u);
	}

	public List<User> getAllUsers() {
		List<User> listUser = new ArrayList<>();
		repo.findAll().forEach(users -> listUser.add(users));
		return   listUser;
	}
	
	public List<User> getAll() {
		List<User> userList = repo.findAll();

		if (userList.size() > 0) {
			return userList;
		} else {
			return new ArrayList<User>();
		}
	}
	
	public User createOrUpdateUser(User entity) {
		Optional<User> user = repo.findById(entity.getId());

		if (user.isPresent()) {
			User newEntity = user.get();
			newEntity.setName(entity.getName());

			newEntity = repo.save(newEntity);

			return newEntity;
		} else {
			entity = repo.save(entity);

			return entity;
		}
	} 
	
	public User getUserById(String id) {
		Optional<User> user = repo.findById(id);

		if (user.isPresent()) {
			return user.get();
		} else {
			 throw new RecordNotFoundException("No User record exist for given id");
		}
		
	}
	
	public String deleteUserById(String id) {
		Optional<User> user = repo.findById(id);

		if (user.isPresent()) {
			repo.deleteById(id);
			return id;
		} else {
			 throw new RecordNotFoundException("No User record exist for given id - " + id);
		}
	}
}
