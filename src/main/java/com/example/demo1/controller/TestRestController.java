package com.example.demo1.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo1.exceptions.RecordNotFoundException;
import com.example.demo1.modal.User;
import com.example.demo1.service.UserService;

@RestController
@RequestMapping("/user")
public class TestRestController {
	
	@Autowired
	UserService userService;
	
	//@GetMapping("/hello")
	@RequestMapping("/add/{name}")
	public ResponseEntity<?> helloWorld(@PathVariable String name) {
		
		return new ResponseEntity<User>(userService.addUser(name),HttpStatus.OK);
	}	
	
	@GetMapping("/allusers")
	public List<User> getAllUsers() {
		
	return userService.getAllUsers();
	}
	
	@GetMapping("/all")
	public ResponseEntity<List<User>> getAll() {
		List<User> list = userService.getAll();

		return new ResponseEntity<List<User>>(list, new HttpHeaders(), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<User> getUserById(@PathVariable String id) {

		return new ResponseEntity<User>(userService.getUserById(id), HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<User> createOrUpdateUser(@RequestBody User user) {
		User updated = userService.createOrUpdateUser(user);
		return new ResponseEntity<User>(updated, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteUserById(@PathVariable("id") String id) {
		
		return new ResponseEntity<String>(userService.deleteUserById(id), HttpStatus.OK);
	}
}