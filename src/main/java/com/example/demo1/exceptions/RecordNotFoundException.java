package com.example.demo1.exceptions;

public class RecordNotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	public RecordNotFoundException(String message) {
		super(message);
	}
	

}
