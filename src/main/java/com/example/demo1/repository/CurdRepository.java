package com.example.demo1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo1.modal.User;

@Repository
public interface CurdRepository extends JpaRepository<User, String> {

	User findUserById(String id);
}
